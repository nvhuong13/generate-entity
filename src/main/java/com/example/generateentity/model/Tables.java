package com.example.generateentity.model;

import java.util.Date;

public class Tables {

    public Tables() {
    }

    public Tables(String tableSchema, String tableName, String tableRows, Date createTime, Date updateTime, String tableComment) {
        this.tableSchema = tableSchema;
        this.tableName = tableName;
        this.tableRows = tableRows;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.tableComment = tableComment;
    }

    private String tableSchema;

    private String tableName;

    private String tableRows;

    private Date createTime;

    private Date updateTime;

    private String tableComment;


    public String getTableSchema() {
        return tableSchema;
    }

    public void setTableSchema(String tableSchema) {
        this.tableSchema = tableSchema;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getTableRows() {
        return tableRows;
    }

    public void setTableRows(String tableRows) {
        this.tableRows = tableRows;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getTableComment() {
        return tableComment;
    }

    public void setTableComment(String tableComment) {
        this.tableComment = tableComment;
    }
}
