package com.example.generateentity.model;

public class Columns {

    public Columns() {
    }

    public Columns(String tableSchema, String tableName, String columnName, String ordinalPosition,
                   String columnDefault, String isNullable, String dataType,
                   String characterMaximunLength, String columnType, String columnKey, String extra, String columnComment) {
        this.tableSchema = tableSchema;
        this.tableName = tableName;
        this.columnName = columnName;
        this.ordinalPosition = ordinalPosition;
        this.columnDefault = columnDefault;
        this.isNullable = isNullable;
        this.dataType = dataType;
        this.characterMaximunLength = characterMaximunLength;
        this.columnType = columnType;
        this.columnKey = columnKey;
        this.extra = extra;
        this.columnComment = columnComment;
    }

    private String tableSchema;

    private String tableName;

    private String columnName;

    private String ordinalPosition;

    private String columnDefault;

    private String isNullable;

    private String dataType;

    private String characterMaximunLength;

    private String columnType;

    private String columnKey;

    private String extra;

    private String columnComment;

    private Integer dataPrecision; // for Oracle


    public String getTableSchema() {
        return tableSchema;
    }

    public void setTableSchema(String tableSchema) {
        this.tableSchema = tableSchema;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getOrdinalPosition() {
        return ordinalPosition;
    }

    public void setOrdinalPosition(String ordinalPosition) {
        this.ordinalPosition = ordinalPosition;
    }

    public String getColumnDefault() {
        return columnDefault;
    }

    public void setColumnDefault(String columnDefault) {
        this.columnDefault = columnDefault;
    }

    public String getIsNullable() {
        return isNullable;
    }

    public void setIsNullable(String isNullable) {
        this.isNullable = isNullable;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getCharacterMaximunLength() {
        return characterMaximunLength;
    }

    public void setCharacterMaximunLength(String characterMaximunLength) {
        this.characterMaximunLength = characterMaximunLength;
    }

    public String getColumnType() {
        return columnType;
    }

    public void setColumnType(String columnType) {
        this.columnType = columnType;
    }

    public String getColumnKey() {
        return columnKey;
    }

    public void setColumnKey(String columnKey) {
        this.columnKey = columnKey;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public String getColumnComment() {
        return columnComment;
    }

    public void setColumnComment(String columnComment) {
        this.columnComment = columnComment;
    }

    public Integer getDataPrecision() {
        return dataPrecision;
    }

    public void setDataPrecision(Integer dataPrecision) {
        this.dataPrecision = dataPrecision;
    }

    public String getDocColumnKey() {
        if ("PRI".equals(columnKey)) {
            return "P";
        }

        if ("MUL".equals(columnKey)) {
            return "F";
        }

        return "";
    }
}
