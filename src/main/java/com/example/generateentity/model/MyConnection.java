package com.example.generateentity.model;

public class MyConnection {
    public String url;
    public String username;
    public String password;
    public String schema;

    public MyConnection(){

    }
    public MyConnection(String url, String username, String password, String schema){
        this.url = url;
        this.username = username;
        this.password = password;
        this.schema = schema;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }
}
