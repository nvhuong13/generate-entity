package com.example.generateentity.controller;

import com.example.generateentity.BasicObject;
import com.example.generateentity.Constant;
import com.example.generateentity.model.MyConnection;
import com.example.generateentity.model.Tables;
import com.example.generateentity.repository.TablesRepository;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletOutputStream;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@RestController
public class DataController {

    @Autowired
    BasicObject basicObject;

    @Autowired
    TablesRepository tablesRepository;

    @GetMapping("/get-entity-from-db")
    public String getEntityFromDB() throws Exception {
        MyConnection conn = new MyConnection();
        conn.setUrl(Constant.URL);
        conn.setSchema(Constant.SCHEMA);
        conn.setUsername(Constant.USERNAME);
        conn.setPassword(Constant.PASSWORD);
        List<Tables> tables = tablesRepository.getAllTable(conn);

        List<String> tablesName = tables.stream()
                .map(element-> element.getTableName())
                .collect(Collectors.toList());

        List<File> lstFileEntity = new ArrayList<>();
        String home = System.getProperty("user.home");
        for (String tableName : tablesName) {
            String fileName = home + "/Downloads/" + basicObject.getPropertyColumn(tableName, true) + "Entity.java";
            lstFileEntity.add(new File(fileName));
            PrintWriter writer = new PrintWriter(fileName, "UTF-8");
            writer.append(basicObject.createBO(tableName));
            writer.close();
        }

        File f = new File(home + "/Downloads/entity.zip");
        ZipOutputStream zipOut = new ZipOutputStream(new FileOutputStream(f));
        for (File file: lstFileEntity) {
            zipOut.putNextEntry(new ZipEntry(file.getName()));
            zipOut.write(Files.readAllBytes(file.toPath()));
            zipOut.closeEntry();

            file.delete();
        }
        zipOut.close();


        return "Saved to Downloads!";
    }
}
