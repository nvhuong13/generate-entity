package com.example.generateentity;

import com.example.generateentity.model.Columns;
import com.example.generateentity.model.MyConnection;
import com.example.generateentity.repository.TablesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BasicObject {
    @Autowired
    TablesRepository tablesRepository;

    public String createBO(String tableName) throws Exception {
        MyConnection connection = new MyConnection();
        connection.setUrl(Constant.URL);
        connection.setUsername(Constant.USERNAME);
        connection.setSchema(Constant.SCHEMA);
        connection.setPassword(Constant.PASSWORD);
        List<Columns> columns = tablesRepository.getColumnByTable(tableName, connection);

        StringBuilder strBO = new StringBuilder();

        strBO.append("import com.fasterxml.jackson.annotation.JsonIgnoreProperties;\n");
        strBO.append("import javax.persistence.AttributeOverride;\n");
        strBO.append("import javax.persistence.Column;\n");
        strBO.append("import javax.persistence.Entity;\n");
        strBO.append("import javax.persistence.Table;\n");
        strBO.append("import java.util.Date;\n\n");
        strBO.append("@Entity\n");
        strBO.append("@Table(name = \"" + tableName +"\")\n");
        strBO.append("public class "+ getPropertyColumn(tableName.toLowerCase(), true) +"Entity {\n");
        strBO.append("\t@Id\n");
        strBO.append("\t@GeneratedValue(strategy = GenerationType.IDENTITY)\n");
        for (Columns column : columns) {
            strBO.append("\t@Column(name = \"" + column.getColumnName() +"\")\n");
            strBO.append("\tprivate " + getDataTypeJava(column) + " " + getPropertyColumn(column.getColumnName(), false) + ";\n\n");
        }

        // Getter
        for (Columns column: columns) {
            strBO.append("\tpublic " + getDataTypeJava(column)+ " get" + getPropertyColumn(column.getColumnName(), true) +"() {\n");
            strBO.append("\t\treturn " + getPropertyColumn(column.getColumnName(), false) +";\n");
            strBO.append("\t}\n\n");
        }

        // Setter
        for (Columns column: columns) {
            strBO.append("\tpublic void set" + getPropertyColumn(column.getColumnName(), true) +"(" + getDataTypeJava(column) + " "+ getPropertyColumn(column.getColumnName(),false) +") {\n");
            strBO.append("\t\tthis." + getPropertyColumn(column.getColumnName(), false) +" = "+ getPropertyColumn(column.getColumnName(), false) +";\n");
            strBO.append("\t}\n\n");
        }

        strBO.append("}");
        return strBO.toString();
    }

    public String getPropertyColumn(String s, boolean uppercaseFirst) {
        s = s.toLowerCase();
        for (int i = 97; i <= 122; i++) {
            s = s.replace("_" + (char)i, "" + (char)(i - 32));
        }
        if (uppercaseFirst) {
            s = Character.toUpperCase(s.charAt(0)) + s.substring(1);
        }
        return s;
    }

    public String getDataTypeJava(Columns column) {
        if ("NUMBER".equals(column.getDataType())) {
            if (column.getDataPrecision().intValue() == 1) {
                return "Integer";
            } else {
                return "Long";
            }
        } else if ("DATE".equals(column.getDataType())) {
            return "Date";
        } else {
            return "String";
        }
    }
}
