package com.example.generateentity.repository;


import com.example.generateentity.model.Columns;
import com.example.generateentity.model.MyConnection;
import com.example.generateentity.model.Tables;

import java.sql.SQLException;
import java.util.List;

public interface TablesRepository {
    List<Tables> getAllTable(MyConnection connection) throws SQLException, ClassNotFoundException;
    List<Columns> getColumnByTable(String tableName, MyConnection connection) throws Exception;
}
