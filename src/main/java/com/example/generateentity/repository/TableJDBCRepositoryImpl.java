package com.example.generateentity.repository;

import com.example.generateentity.model.Columns;
import com.example.generateentity.model.MyConnection;
import com.example.generateentity.model.Tables;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class TableJDBCRepositoryImpl implements TablesRepository {

    @Override
    public List<Tables> getAllTable(MyConnection connection) throws SQLException, ClassNotFoundException {
        Connection conn = DriverManager.getConnection(connection.getUrl(), connection.getUsername(), connection.getPassword());

        PreparedStatement statement = conn.prepareStatement(
                "SELECT TABLE_SCHEMA, UPPER(TABLE_NAME), TABLE_ROWS, CREATE_TIME, UPDATE_TIME, TABLE_COMMENT \n" +
                        "FROM INFORMATION_SCHEMA.TABLES \n" +
                        "WHERE TABLE_SCHEMA = ?"
                        + " " +
                        "ORDER BY UPPER(TABLE_NAME)");

        statement.setString(1, connection.getSchema());

        ResultSet rs = statement.executeQuery();

        List<Tables> list = new ArrayList<>();
        Tables table = null;

        while (rs.next()) {
            table = new Tables();
            table.setTableSchema(rs.getString("TABLE_SCHEMA"));
            table.setTableName(rs.getString("UPPER(TABLE_NAME)"));
            table.setTableRows(rs.getString("TABLE_ROWS"));
            table.setCreateTime(rs.getDate("CREATE_TIME"));
            table.setUpdateTime(rs.getDate("UPDATE_TIME"));
            table.setTableComment(rs.getString("TABLE_COMMENT"));
            list.add(table);
        }
        conn.close();
        return list;
    }
    @Override
    public List<Columns> getColumnByTable(String tableName, MyConnection connection) throws SQLException {
        Connection conn = DriverManager.getConnection(connection.getUrl(), connection.getUsername(), connection.getPassword());

        PreparedStatement statement = conn.prepareStatement(
                "SELECT TABLE_SCHEMA, \n" +
                        "TABLE_NAME, \n" +
                        "COLUMN_NAME, \n" +
                        "ORDINAL_POSITION, \n" +
                        "COLUMN_DEFAULT, \n" +
                        "IS_NULLABLE, \n" +
                        "DATA_TYPE, \n" +
                        "CHARACTER_MAXIMUM_LENGTH, \n" +
                        "COLUMN_TYPE, " +
                        "COLUMN_KEY, \n" +
                        "EXTRA, \n" +
                        "COLUMN_COMMENT\n" +
                        "FROM INFORMATION_SCHEMA.COLUMNS\n" +
                        "WHERE UPPER(TABLE_NAME) = ? and table_schema = ?");

        statement.setString(1, tableName);
        statement.setString(2, connection.getSchema());

        ResultSet rs = statement.executeQuery();

        List<Columns> list = new ArrayList<>();
        Columns column = null;

        while (rs.next()) {
            column = new Columns();
            column.setTableName(rs.getString("TABLE_NAME"));
            column.setColumnName(rs.getString("COLUMN_NAME"));
            column.setOrdinalPosition(rs.getString("ORDINAL_POSITION"));
            column.setColumnDefault(rs.getString("COLUMN_DEFAULT"));
            column.setIsNullable(rs.getString("IS_NULLABLE"));
            column.setDataType(rs.getString("DATA_TYPE"));
            column.setCharacterMaximunLength(rs.getString("CHARACTER_MAXIMUM_LENGTH"));
            column.setColumnType(rs.getString("COLUMN_TYPE"));
            column.setColumnKey(rs.getString("COLUMN_KEY"));
            column.setExtra(rs.getString("EXTRA"));
            column.setColumnComment(rs.getString("COLUMN_COMMENT"));
            list.add(column);
        }
        conn.close();
        return list;
    }


}
