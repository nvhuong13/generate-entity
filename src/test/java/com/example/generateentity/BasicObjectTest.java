package com.example.generateentity;

import com.example.generateentity.model.Columns;
import com.example.generateentity.repository.TablesRepository;
import org.junit.Test;
import org.junit.jupiter.api.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.class)
public class BasicObjectTest {

    @InjectMocks
    BasicObject basicObject;
    @Mock
    TablesRepository tablesRepository;

    @BeforeAll
    static void setup() {
        System.out.println("Before all");
    }

    @AfterAll
    public static void afterAll() {
        System.out.println("afterAll");
    }

    @BeforeEach
    public void beforeEach() {
        System.out.println("BeforeEach");
    }

    @AfterEach
    public void afterEach() {
        System.out.println("afterEach");
    }

    @Test
    @DisplayName("Test generate convert column name to variable java")
    public void test1() {
        String result1 = basicObject.getPropertyColumn("TT_PAKD", true);
        String result2 = basicObject.getPropertyColumn("FULL_NAME", true);
        String result3 = basicObject.getPropertyColumn("PASS_WORD", true);

        String result4 = basicObject.getPropertyColumn("TT_PAKD", false);
        String result5 = basicObject.getPropertyColumn("FULL_NAME", false);
        String result6 = basicObject.getPropertyColumn("PASS_WORD", false);

        // compare
        assertEquals("TtPakd", result1);
        assertTrue("FullName".equals(result2));
        assertTrue("PassWord".equals(result3));

        assertEquals("ttPakd", result4);
        assertTrue("fullName".equals(result5));
        assertTrue("passWord".equals(result6));
    }

    @Test
    @DisplayName("Test convert data type column to data type variable")
    public void test2() {
        Columns columns = new Columns();
        columns.setDataType("NUMBER");
        columns.setDataPrecision(1);
        String result1 = basicObject.getDataTypeJava(columns);
        columns.setDataType("DATE");
        String result2 = basicObject.getDataTypeJava(columns);
        columns.setDataType("STRING");
        String result3 = basicObject.getDataTypeJava(columns);
        assertTrue("Integer".equals(result1));
        assertTrue("Date".equals(result2));
        assertTrue("String".equals(result3));
    }

    @Test
    @DisplayName("Test create text BO")
    @Disabled
    public void test3() throws Exception {
        Columns columns0 = new Columns();
        columns0.setColumnName("ID");
        columns0.setDataType("NUMBER");
        columns0.setDataPrecision(0);

        Columns columns1 = new Columns();
        columns1.setColumnName("ORGANIZATION_ID");
        columns1.setDataType("NUMBER");
        columns1.setDataPrecision(1);

        Columns columns2 = new Columns();
        columns2.setColumnName("PROJECT_NAME");
        columns2.setDataType("OtherDataType");
        columns2.setDataPrecision(1);

        List<Columns> mockLstColumns = new ArrayList<>();
        mockLstColumns.add(columns0);
        mockLstColumns.add(columns1);
        mockLstColumns.add(columns2);

        Mockito.when(tablesRepository.getColumnByTable(any(), any())).thenReturn(mockLstColumns);
//        Mockito.when(tablesRepository.getColumnByTable(any(), any())).thenReturn(<call_method_first>).thenReturn(<call_method_second>);
        String result = basicObject.createBO("TT_PAKD");
        // check log to result
        System.out.println(result);
    }
}
